From: Colin Watson <cjwatson@debian.org>
Date: Sat, 1 Mar 2025 20:44:21 +0000
Subject: Fix tests with Click 8.2

https://github.com/pallets/click/pull/2523 introduced changes to
`click.testing.Result` that broke a few unit tests in celery.  Although
this Click version hasn't been fully released yet, this adjusts Celery
to work with both old and new versions.

Forwarded: https://github.com/celery/celery/pull/9590
Bug-Debian: https://bugs.debian.org/1098539
Last-Update: 2025-03-01
---
 t/unit/app/test_preload_cli.py | 4 ++--
 t/unit/bin/test_control.py     | 8 ++++----
 2 files changed, 6 insertions(+), 6 deletions(-)

diff --git a/t/unit/app/test_preload_cli.py b/t/unit/app/test_preload_cli.py
index 9932f5b..cb07b78 100644
--- a/t/unit/app/test_preload_cli.py
+++ b/t/unit/app/test_preload_cli.py
@@ -38,7 +38,7 @@ def test_preload_options(subcommand_with_params: Tuple[str, ...], isolated_cli_r
         catch_exceptions=False,
     )
 
-    assert "No such option: --ini" in res_without_preload.stdout
+    assert "No such option: --ini" in res_without_preload.output
     assert res_without_preload.exit_code == 2
 
     res_with_preload = isolated_cli_runner.invoke(
@@ -53,4 +53,4 @@ def test_preload_options(subcommand_with_params: Tuple[str, ...], isolated_cli_r
         catch_exceptions=False,
     )
 
-    assert res_with_preload.exit_code == 0, res_with_preload.stdout
+    assert res_with_preload.exit_code == 0, res_with_preload.output
diff --git a/t/unit/bin/test_control.py b/t/unit/bin/test_control.py
index 6d3704e..74f6e4f 100644
--- a/t/unit/bin/test_control.py
+++ b/t/unit/bin/test_control.py
@@ -33,8 +33,8 @@ def test_custom_remote_command(celery_cmd, custom_cmd, isolated_cli_runner: CliR
         [*_GLOBAL_OPTIONS, celery_cmd, *_INSPECT_OPTIONS, *custom_cmd],
         catch_exceptions=False,
     )
-    assert res.exit_code == EX_UNAVAILABLE, (res, res.stdout)
-    assert res.stdout.strip() == 'Error: No nodes replied within time constraint'
+    assert res.exit_code == EX_UNAVAILABLE, (res, res.output)
+    assert res.output.strip() == 'Error: No nodes replied within time constraint'
 
 
 @pytest.mark.parametrize(
@@ -54,8 +54,8 @@ def test_unrecognized_remote_command(celery_cmd, remote_cmd, isolated_cli_runner
         [*_GLOBAL_OPTIONS, celery_cmd, *_INSPECT_OPTIONS, remote_cmd],
         catch_exceptions=False,
     )
-    assert res.exit_code == 2, (res, res.stdout)
-    assert f'Error: Command {remote_cmd} not recognized. Available {celery_cmd} commands: ' in res.stdout
+    assert res.exit_code == 2, (res, res.output)
+    assert f'Error: Command {remote_cmd} not recognized. Available {celery_cmd} commands: ' in res.output
 
 
 _expected_inspect_regex = (
